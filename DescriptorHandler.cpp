#include "Descriptor.h"
#include "DescriptorBuilder.h"
#include "DescriptorFinder.h"
#include "DescriptorHandler.h"

DescriptorHandler::DescriptorHandler(DescriptorBuilder *builder, DescriptorFinder *finder) {
	/*if (!finder->isTypeAcceptable(builder->getType())) {
		throw std::invalid_argument("Not acceptable builder for finder");
	}*/

	DescriptorHandler::builder = builder;
	DescriptorHandler::finder = finder;
}
std::vector<int> *DescriptorHandler::getType() {
	return builder->getType();
}
void DescriptorHandler::add(Descriptor *desc) {
	finder->add(desc);
}
Descriptor *DescriptorHandler::add(cv::Mat *image) {
	Descriptor* desc = builder->build(image);
	finder->add(desc);
	return desc;
}
Descriptor *DescriptorHandler::find(Descriptor *desc) {
	return finder->find(desc);
}
Descriptor *DescriptorHandler::find(cv::Mat *image) {
	return finder->find(builder->build(image));
}
void DescriptorHandler::remove(int id) {
	finder->remove(id);
}