#include "ImageCorrupt.h"

using namespace cv;

Mat gaussianNoise(Mat src, int mean, int stddev)
{
	Mat gaussian_noise = src.clone();
	randn(gaussian_noise,mean,stddev);
	return src + gaussian_noise;
}

Mat saltPepperNoise(Mat src, int maxToBlack, int minToWhite) {
	Mat saltpepper_noise = Mat::zeros(src.rows, src.cols, CV_8U);	
	randu(saltpepper_noise, 0, 255);
	Mat black = saltpepper_noise < maxToBlack;
	Mat white = saltpepper_noise > minToWhite;
	Mat saltpepper_img = src.clone();
	saltpepper_img.setTo(255, white);
	saltpepper_img.setTo(0, black);
	return saltpepper_img;
}

Mat blur(Mat src, int kernelSize) {
	Mat blurred = src.clone();
	cv::blur(src, blurred, Size(kernelSize, kernelSize));
	return blurred;
}

Mat border(Mat src, int borderWidth, int borderHeight) {
	Mat bordered = Mat::zeros(src.rows + 2 * borderWidth, src.cols + 2 * borderHeight, src.type());
	for (int i = 0; i < src.rows; i++) {
		for (int j = 0; j < src.cols; j++) {
			bordered.at<cv::Vec3b>(i + borderWidth, j + borderHeight) = src.at<cv::Vec3b>(i, j);
		}
	}
	return bordered;
}

Mat rotate(Mat src, double angle, double scale) {
    Mat rotated = Mat::zeros(src.rows, src.cols, src.type());
	Point center = Point(src.cols / 2, src.rows / 2);

    /// Get the rotation matrix with the specifications above
	Mat rot_mat = getRotationMatrix2D( center, angle, scale );

    /// Rotate the warped image
	warpAffine( src, rotated, rot_mat, rotated.size() );
	return rotated;
}

Mat warp(Mat src, Point2f transformed[]) {
	Mat warped = Mat::zeros(src.rows, src.cols, src.type());
	Point2f srcTri[3];
	srcTri[0] = Point2f( 0,0 );
	srcTri[1] = Point2f( src.cols - 1, 0 );
	srcTri[2] = Point2f( 0, src.rows - 1 );
	Mat warp_mat = getAffineTransform( srcTri, transformed );
	warpAffine( src, warped, warp_mat, warped.size() );
	return warped;
}

Mat flash(Mat src, Point2f center, int maxBrightAddition, int maxBrightSubstraction, int brightingRadius) {
	double maxDist = norm(Point2f(0, 0) - center);
	double tmpDist = norm(Point2f(src.cols - 1, 0) - center);
	if (maxDist < tmpDist) maxDist = tmpDist;
	tmpDist = norm(Point2f(src.cols - 1, src.rows - 1) - center);
	if (maxDist < tmpDist) maxDist = tmpDist;
	tmpDist = norm(Point2f(0, src.rows - 1) - center);
	if (maxDist < tmpDist) maxDist = tmpDist;
	Mat flashed = Mat::zeros(src.rows, src.cols, src.type());
	double currDist, coeff, distCoeff = (double)brightingRadius / maxDist;
	int r, g, b;
	for (int i = 0; i < src.rows; i++) {
		for (int j = 0; j < src.cols; j++) {
			currDist = norm(Point2f(i, j) - center);
			Vec3b vec = src.at<Vec3b>(i, j);
			if (currDist < brightingRadius) {
				coeff = (1 - (currDist / brightingRadius)) * maxBrightAddition;
			} else {
				coeff = -((currDist - brightingRadius) / (maxDist - brightingRadius)) * maxBrightSubstraction;
			}
			r = vec[0] + std::floor(coeff);
			g = vec[1] + std::floor(coeff);
			b = vec[2] + std::floor(coeff);
			if (r > 255) r = 255;
			if (r < 0) r = 0;
			if (g > 255) g = 255;
			if (g < 0) g = 0;
			if (b > 255) b = 255;
			if (b < 0) b = 0;
			flashed.at<Vec3b>(i, j) = Vec3b(r, g, b);
		}
	}
	return flashed;
}

Mat bright(Mat src, double alpha, int beta) {
	Mat brighted = Mat::zeros(src.rows, src.cols, src.type());
	Vec3b tmp;
	int r, g, b;
	for (int i = 0; i < src.rows; i++) {
		for (int j = 0; j < src.cols; j++) {
			Vec3b vec = src.at<Vec3b>(i, j);
			b = alpha * vec[0] + beta;
			g = alpha * vec[1] + beta;
			r = alpha * vec[2] + beta;
			if (r > 255) r = 255;
			if (r < 0) r = 0;
			if (g > 255) g = 255;
			if (g < 0) g = 0;
			if (b > 255) b = 255;
			if (b < 0) b = 0;
			brighted.at<Vec3b>(i, j) = Vec3b(b, g, r);
			
		}
	}
	return brighted;
}