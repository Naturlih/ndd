#include "Descriptor.h"
#include "DescriptorBuilder.h"
#include "DescriptorFinder.h"
#include <opencv2/core/core.hpp>
#pragma once
class DescriptorHandler
{
public:
	DescriptorHandler(DescriptorBuilder *builder, DescriptorFinder *finder);
	std::vector<int> *getType();
	void add(Descriptor *desc);
	Descriptor *add(cv::Mat *image);
	Descriptor *find(Descriptor *desc);
	Descriptor *find(cv::Mat *image);
	void remove(int id);
	~DescriptorHandler(void) {
		delete builder;
		delete finder;
	}
private :
	DescriptorBuilder *builder;
	DescriptorFinder *finder;
};