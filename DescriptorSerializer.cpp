#include "DescriptorSerializer.h"
#include "SIFTDescriptorBuilder.h"

std::vector<Descriptor *> *DescriptorSerializer::deserialize(std::string path, int pointCount) {
	std::vector<Descriptor *> *vector = new std::vector<Descriptor *>();
	std::ifstream myfile (path);
	std::string line;
	if (myfile.is_open())
	{
		getline(myfile,line);
		std::vector<std::string> typeVec = split(line, ' ');
		while ( getline(myfile,line) )
		{
			std::vector<int> *desc = new std::vector<int>();
			int id = 0;
			int tmp = 0;
			int i = 0;
			while (line[i] != ' ') {
				id = id * 10 + (line[i] - '0');
				i++;
			}
			i++;
			bool isNegative;
			while (i < line.length()) {
				tmp = 0;
				isNegative = false;
				if (line[i] == '-') {
					isNegative = true;
					i++;
				}
				while (i < line.length() && line[i] != '|') {
					tmp = tmp * 10 + (line[i] - '0');
					i++;
				}
				if (isNegative) {
					tmp = -tmp;
				}
				desc->push_back(tmp);
				i++;
				if (desc->size() >= 130 * pointCount) {
					break;
				}
			}
			vector->push_back(new Descriptor(id, *desc));
			delete desc;
			if (id == 17000) {
				break;
			}
		}
		myfile.close();
	} else printf("Unable to open file"); 
	return vector;
}

void DescriptorSerializer::deserializeAndFill(DescriptorFinder *finder, std::string path, int pointCount) {
	std::ifstream myfile (path);
	std::string line;
	if (myfile.is_open())
	{
		getline(myfile,line);
		std::vector<std::string> typeVec = split(line, ' ');
		std::vector<int> type;
		for (int i = 0; i < typeVec.size(); i++) {
			type.push_back(atoi(typeVec.at(i).c_str()));
		}
		while ( getline(myfile,line) )
		{
			std::vector<int> *desc = new std::vector<int>();
			int id = 0;
			int tmp = 0;
			int i = 0;
			while (line[i] != ' ') {
				id = id * 10 + (line[i] - '0');
				i++;
			}
			i++;
			bool isNegative;
			while (i < line.length()) {
				tmp = 0;
				isNegative = false;
				if (line[i] == '-') {
					isNegative = true;
					i++;
				}
				while (i < line.length() && line[i] != '|') {
					tmp = tmp * 10 + (line[i] - '0');
					i++;
				}
				if (isNegative) {
					tmp = -tmp;
				}
				desc->push_back(tmp);
				i++;
				if (desc->size() >= 130 * pointCount) {
					break;
				}
			}
			(*finder).add(new Descriptor(id, *desc));
			delete desc;
		}
		myfile.close();
	} else printf("Unable to open file");
}

void DescriptorSerializer::serialize(std::vector<Descriptor*> *descs, std::vector<int> *type, std::string path) {
	std::ofstream descFile;
	descFile.open (path);
	bool split = false;
	for (int i = 0; i < (*type).size(); i++) {
		if (split) {
			descFile << " ";
		}
		descFile << (*type)[i];
		split = true;
	}
	split = false;
	descFile << '\n';
	for (int i = 0; i < (*descs).size(); i++) {
		Descriptor *desc = (*descs).at(i);
		if (split) {
			descFile << '\n';
		}
		descFile << (*desc).getId();
		//TODO
		descFile << " ";
		std::vector<int> value = (*desc).getVal();
		for (int j = 0; j < value.size(); j++) {
			descFile << (j == 0 ? "" : "|") << value[j];
		}
		split = true;
	}
	descFile.close();
}

std::vector<std::string> &DescriptorSerializer::split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> DescriptorSerializer::split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

DescriptorSerializer::DescriptorSerializer(void) {}
DescriptorSerializer::~DescriptorSerializer(void) {}
