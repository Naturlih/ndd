#pragma once
#include "descriptorfinder.h"
#include "Log.h"
#include "LSH.h"
using namespace std;
class LSHDescriptorFinder : public DescriptorFinder
{
public:
	LSHDescriptorFinder(int angleDist, vector<vector<vector<int>*>*> masks, int bucketsCount, int descLength);
	bool isTypeAcceptable(std::vector<int> *type);
	void add(Descriptor *desc);
	void remove(int id);
	Descriptor *find(Descriptor *desc);
	std::vector<Descriptor*> *getDescs();
	int getMaxId();
	~LSHDescriptorFinder();
	int threshhold;
private:
	vector<vector<LSH*>> buckets;
	int angleDist;
	int descLength;
	vector<double> angleSpaces;
	vector<Descriptor *> descList;
	vector<vector<vector<int>*>*> masks;
};
