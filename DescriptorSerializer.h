#include "DescriptorBuilder.h"
#include "DescriptorFinder.h"
#include <iostream>
#include <fstream>
#include <sstream>
#pragma once
class DescriptorSerializer
{
public:
	static std::vector<Descriptor *> *deserialize(std::string path, int pointCount = 100);
	static void deserializeAndFill(DescriptorFinder *finder, std::string path, int pointCount = 100);
	static void serialize(std::vector<Descriptor*> *descs, std::vector<int> *type, std::string path);
private:
	static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
	static std::vector<std::string> split(const std::string &s, char delim);
	DescriptorSerializer(void);
	~DescriptorSerializer(void);
};
