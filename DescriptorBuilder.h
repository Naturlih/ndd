#include "Descriptor.h"
#include <opencv2/core/core.hpp>
#pragma once
class DescriptorBuilder
{
public:
	virtual std::vector<int> *getType() = 0;
	virtual Descriptor *build(cv::Mat *image) = 0;
	virtual int getDescLength() = 0;
};