#pragma once
#include "SIFTDescriptorBuilder.h"

SIFTDescriptorBuilder::SIFTDescriptorBuilder(int pointNumber, int res) {
	SIFTDescriptorBuilder::descriptorId = 0;
	type = new std::vector<int>;
	type->push_back(3);
	r = res;
	featureDetector = new cv::SiftFeatureDetector(pointNumber);
	featureExtractor = cv::DescriptorExtractor::create("SIFT");
}
Descriptor *SIFTDescriptorBuilder::build(cv::Mat *image) {
	cv::Mat resized;
	cv::resize(*image, resized, cv::Size(r, r));
	std::vector<cv::KeyPoint> keypoints;
	SIFTDescriptorBuilder::featureDetector->detect(resized, keypoints);
	cv::Mat descriptors;
	SIFTDescriptorBuilder::featureExtractor->compute(resized, keypoints, descriptors);
	std::vector<int> desc;
	for (int i = 0; i < descriptors.rows; i++) {
		desc.push_back(floor(keypoints[i].angle + 0.5));
		desc.push_back(floor(keypoints[i].size * 10 + 0.5));
		for (int j = 0; j < descriptors.cols; j++) {
			float c = descriptors.at<float>(i, j);
			desc.push_back(floor(c + 0.5));	
		}
	}
	return new Descriptor(SIFTDescriptorBuilder::descriptorId++, desc);
}
std::vector<int> *SIFTDescriptorBuilder::getType() {
	return type;
}
SIFTDescriptorBuilder::~SIFTDescriptorBuilder() {
	delete type;
}
int SIFTDescriptorBuilder::getDescLength() {
	return 130;
}