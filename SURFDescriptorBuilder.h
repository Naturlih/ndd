#pragma once
#include "DescriptorBuilder.h"
#include <opencv2/core/core.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include "opencv2/imgproc/imgproc.hpp"
class SURFDescriptorBuilder : public DescriptorBuilder
{
public:
	SURFDescriptorBuilder(int pointNumber = 50, int res = 256);
	Descriptor *build(cv::Mat *image);
	std::vector<int> *getType();
	int getDescLength();
	~SURFDescriptorBuilder();
private:
	int descriptorId;
	std::vector<int> *type;
	int r;
	int pointNumber;
	cv::SurfFeatureDetector *featureDetector;
	cv::Ptr<cv::DescriptorExtractor> featureExtractor;
};

