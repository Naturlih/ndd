#pragma once
#include "descriptorfinder.h"
using namespace std;
class SIFTDescriptorFinder : public DescriptorFinder
{
public:
	SIFTDescriptorFinder(int angleDist, int descLength);
	bool isTypeAcceptable(std::vector<int> *type);
	void add(Descriptor *desc);
	void remove(int id);
	Descriptor *find(Descriptor *desc);
	int getMaxId();
	std::vector<Descriptor*> *getDescs();
	~SIFTDescriptorFinder();
private:
	int angleDist;
	int descLength;
	vector<Descriptor *> descList;
	vector<double> angleSpaces;
	vector<vector<Descriptor*>> descs;
	double descDistance(int *d1, int *d2);
	bool findBest(int index, double *bestDist, int *bestInd, int *val);
};

