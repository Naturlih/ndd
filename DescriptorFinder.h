#include "Descriptor.h"
#pragma once
class DescriptorFinder
{
public:
	virtual bool isTypeAcceptable(std::vector<int> *type) = 0;
	virtual void add(Descriptor *desc) = 0;
	virtual void remove(int id) = 0;
	virtual int getMaxId() = 0;
	virtual Descriptor *find(Descriptor *desc) = 0;
	virtual ~DescriptorFinder() {};
};