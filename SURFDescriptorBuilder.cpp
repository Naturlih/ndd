#pragma once
#include "SURFDescriptorBuilder.h"

SURFDescriptorBuilder::SURFDescriptorBuilder(int pointNumber, int res) {
	SURFDescriptorBuilder::descriptorId = 0;
	type = new std::vector<int>;
	type->push_back(3);
	r = res;
	SURFDescriptorBuilder::pointNumber = pointNumber;
	featureDetector = new cv::SurfFeatureDetector(400);
	featureExtractor = new cv::SurfDescriptorExtractor(400);;
}
Descriptor *SURFDescriptorBuilder::build(cv::Mat *image) {
	cv::Mat resized;
	cv::resize(*image, resized, cv::Size(r, r));
	std::vector<cv::KeyPoint> keypoints;
	SURFDescriptorBuilder::featureDetector->detect(resized, keypoints);
	cv::Mat descriptors;
	SURFDescriptorBuilder::featureExtractor->compute(resized, keypoints, descriptors);
	std::vector<int> desc;
	for (int i = 0; i < (pointNumber < descriptors.rows ? pointNumber : descriptors.rows); i++) {
		desc.push_back(floor(keypoints[i].angle + 0.5));
		desc.push_back(floor(keypoints[i].size * 10 + 0.5));
		std::vector<float> vals;
		for (int j = 0; j < 128; j++) {
			vals.push_back(descriptors.at<float>(i, j));
		}
		double mean = 0;
		for (int j = 0; j < 128; j++) {
			mean += vals[j] * vals[j];
		}
		mean = sqrt(mean);
		for (int j = 0; j < 128; j++) {
			vals[j] = (vals[j] * 512) / mean;
			desc.push_back(floor(vals[j] + 0.5));
		}
	}
	return new Descriptor(SURFDescriptorBuilder::descriptorId++, desc);
}
std::vector<int> *SURFDescriptorBuilder::getType() {
	return type;
}
SURFDescriptorBuilder::~SURFDescriptorBuilder() {
	delete type;
}
int SURFDescriptorBuilder::getDescLength() {
	return 130;
}