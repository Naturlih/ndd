#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/core/core.hpp>

#pragma once
cv::Mat gaussianNoise(cv::Mat src, int mean, int stddev);
//algo - for each points takes random number in [0..255] integer, then if it is < maxToBlack point will become black, if > minToWhite it will become white
cv::Mat saltPepperNoise(cv::Mat src, int maxToBlack, int minToWhite);
cv::Mat blur(cv::Mat src, int kernelSize);
cv::Mat border(cv::Mat src, int borderWidth, int borderHeight);
cv::Mat rotate(cv::Mat src, double angle, double scale);
///transformed (0,0), (i.cols - 1,0), (0, i.rows - 1) respectively
cv::Mat warp(cv::Mat src, cv::Point2f transformed[]);
//if radius < brightingRadius, then pixel will become brighter, else darker
cv::Mat flash(cv::Mat src, cv::Point2f center, int maxBrightAddition, int maxBrightSubstraction, int brightingRadius);
// newColor = oldColor * alpha + beta
cv::Mat bright(cv::Mat src, double alpha, int beta);