// ImageFramework.cpp: ������� ���� �������.

#include <ctime>
#include <opencv2/core/core.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include "ImageCorrupt.h"
#include "DescriptorSerializer.h"
#include "DescriptorBuilder.h"
#include "SIFTDescriptorFinder.h"
#include "SIFTDescriptorBuilder.h"
#include "DescriptorHandler.h"
#include <stdlib.h>
#include "LSHDescriptorFinder.h"
#include "windows.h"
#include "SURFDescriptorBuilder.h"

#include <time.h>
vector<vector<vector<int>*>*> getMasks(int count);
int main() {
	string path = "D:/";
	std::ostringstream s;
	SIFTDescriptorBuilder *builder = new SIFTDescriptorBuilder();
	LSHDescriptorFinder *finder = new LSHDescriptorFinder(5, getMasks(40), 127, builder->getDescLength());
	DescriptorHandler handler(builder, finder);
	for (int i = 0; i < 10; i++) {
		s.str("");
		s.clear();
		s << path << "example/" << i << ".jpeg";
		handler.add(&cv::imread(s.str()));
	}
	s.str("");
	s.clear();
	s << path << "example/example.jpeg";
	cv::Mat example = cv::imread(s.str());
	s.str("");
	s.clear();
	s << path << "example/7.jpeg";
	cv::Mat orig = cv::imread(s.str());
	Descriptor *found = handler.find(&example);
	s.str("");
	s.clear();
	s << path << "example/" << found->getId() << ".jpeg";
	cv::imshow("Example", example);
	cv::imshow("Original", orig);
	cv::imshow("Found", cv::imread(s.str()));
	cv::waitKey();
	return 0;
}

vector<vector<vector<int>*>*> getMasks(int count) {
	int vecLen = 128;
	vector<vector<vector<int>*>*> masks(count);
	vector<int> tmp(masks.size());
	for (int i = 0; i < masks.size(); i++) {
		tmp[i] = i * 2 + 1;
	}
	for (int i = 0; i < masks.size(); i++) {
		vector<vector<int>*> *vec = new vector<vector<int>*>(vecLen);
		for (int j = 0; j < vecLen; j++) {
			(*vec)[j] = new vector<int>();
		}
		for (int j = 0; j < 1; j++) {
			for (int k = 0; k <= 12; k++) {
				//(*vec)[j * i % 128]->push_back((i + j * 5 + k * 3) % + 28);
				(*vec)[((k + 1) * tmp[i]) % vecLen]->push_back(6);
				(*vec)[((k + 1) * tmp[i]) % vecLen]->push_back(7);
			}
		}
		masks[i] = vec;
	}
	return masks;
}
