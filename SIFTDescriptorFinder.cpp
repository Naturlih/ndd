#include "SIFTDescriptorFinder.h"
#include "DescriptorBuilder.h"

SIFTDescriptorFinder::SIFTDescriptorFinder(int angleDist, int descLength) {
	if (!(360 % angleDist == 0)) {
		throw new exception();
	}
	SIFTDescriptorFinder::descLength = descLength;
	int curr = 0;
	SIFTDescriptorFinder::angleDist = angleDist;
	while (curr < 360) {
		angleSpaces.push_back(curr);
		vector<Descriptor *> tmp;
		descs.push_back(tmp);
		curr += angleDist;
	}
}
bool SIFTDescriptorFinder::isTypeAcceptable(std::vector<int> *type) {
	return false;
}
void SIFTDescriptorFinder::add(Descriptor *desc) {
	descList.push_back(desc);
	vector<int> val = desc->getVal();
	for (int i = 0; i < val.size() / descLength; i++) {
		int angle = val[i * descLength];
		if (angle == 360) {
			angle = 359;
		}
		int index = angle / angleDist;
		descs[index].push_back(new Descriptor(desc->getId(), vector<int>(val.begin() + i * descLength, val.begin() + (i + 1) * descLength)));
	}
}

void SIFTDescriptorFinder::remove(int id) {
	throw new std::exception();
}
Descriptor *SIFTDescriptorFinder::find(Descriptor *desc) {
	vector<int> vval = desc->getVal();
	double bestDist;
	int bestInd = -1;
	int bestIndex;
	bool changeBestIndex = false;
	//TODO serious shitty code
	int ids[20000];
	for (int i = 0; i < 20000; i++) {
		ids[i] = 0;
	}
	for (int i = 0; i < vval.size() / descLength; i++) {		
		bestDist = 1e300;
		int angle = vval[i * descLength];
		if (angle == 360) {
			angle = 359;
		}
		int index = angle / angleDist;
		bestIndex = index;
		int *val = &vval[i * descLength + 2];
		findBest(index, &bestDist, &bestInd, val);
		if (index > 0) {
			changeBestIndex = findBest(index - 1, &bestDist, &bestInd, val);
			if (changeBestIndex) {
				bestIndex = index - 1;
			}
		} else {
			changeBestIndex = findBest(descs.size() - 1, &bestDist, &bestInd, val);
			if (changeBestIndex) {
				bestIndex = descs.size() - 1;
			}
		}
		if (index < descs.size() - 1) {
			changeBestIndex = findBest(index + 1, &bestDist, &bestInd, val);
			if (changeBestIndex) {
				bestIndex = index + 1;
			}
		} else {
			changeBestIndex = findBest(0, &bestDist, &bestInd, val);
			if (changeBestIndex) {
				bestIndex = 0;
			}
		}
		//TODO
		if (bestInd != -1) {//bestDist < 800) {
			ids[descs[bestIndex][bestInd]->getId()]++;
		}
		bestInd = -1;
	}
	int bestCount = -1, bestId;
	for (int i = 0; i < 20000; i++) {
		if (bestCount < ids[i]) {
			bestId = i;
			bestCount = ids[i];
		}
	}
	return descList[bestId];
}

bool SIFTDescriptorFinder::findBest(int index, double *bestDist, int *bestInd, int *val) {
	double currDist;
	bool found = false;
	for (int i = 0; i < descs[index].size(); i++) {
		currDist = descDistance(val, &(descs[index][i]->getVal())[2]);
		if (currDist < *bestDist) {
			*bestDist = currDist;
			*bestInd = i;
			found = true;
		}
	}
	return found;
}
double SIFTDescriptorFinder::descDistance(int *d1, int *d2) {
	double dist = 0;
	double tmp;
	int c = 0;
	for (int i = 0; i < descLength - 2; i++) {
		if (d1[i] == 0 || d2[i] == 0) {
			continue;
		}
		c++;
		tmp = d1[i] - d2[i];
		dist += tmp * tmp;
	}
	return dist / c;
}
/*
double SIFTDescriptorFinder::descDistance(std::vector<int> *d1, std::vector<int> *d2) {
	double dist = 0;
	double tmp;
	int c = 0;
	for (int i = 0; i < (*d1).size(); i++) {
		if ((*d1)[i] == 0 || (*d2)[i] == 0) {
			continue;
		}
		c++;
		tmp = (*d1)[i] - (*d2)[i];
		dist += tmp * tmp;
	}
	return dist / c;
}
bool SIFTDescriptorFinder::findBest(int index, double *bestDist, int *bestInd, vector<int> *val) {
	double currDist;
	bool found = false;
	for (int i = 0; i < descs[index].size(); i++) {
		currDist = descDistance(val, &(descs[index][i]->getVal()));
		if (currDist < *bestDist) {
			*bestDist = currDist;
			*bestInd = i;
			found = true;
		}
	}
	return found;
}
*/
std::vector<Descriptor*> *SIFTDescriptorFinder::getDescs() {
	return &descs[0];
}

int SIFTDescriptorFinder::getMaxId() {
	int maxId = -1;
	for (int i = descList.size() - 1; i >= 0; i--) {
		if (maxId < descList[i]->getId()) {
			maxId = descList[i]->getId();
		}
	}
	return maxId;
}

SIFTDescriptorFinder::~SIFTDescriptorFinder() {
	for (int i = 0; i < descList.size(); i++) {
		delete descList[i];
	}
	for (int i = 0; i < descs.size(); i++) {
		for (int j = 0; j < descs[i].size(); j++) {
			delete descs[i][j];
		}
	}
	angleSpaces.clear();
}