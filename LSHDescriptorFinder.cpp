#include "LSHDescriptorFinder.h"
#include "DescriptorBuilder.h"
void incIds(vector<Descriptor *> *idsToAdd, vector<int> *ids);

//TODO
#include <iostream>
#include <fstream>
#include <sstream>

LSHDescriptorFinder::LSHDescriptorFinder(int angleDist, vector<vector<vector<int>*>*> masks, int bucketsCount, int descLength) {
	if (!(360 % angleDist == 0)) {
		throw new exception();
	}
	LSHDescriptorFinder::masks = masks;
	LSHDescriptorFinder::threshhold = 2000;
	LSHDescriptorFinder::descLength = descLength;
	double curr = 0;
	LSHDescriptorFinder::angleDist = angleDist;
	while (curr < 360) {
		angleSpaces.push_back(curr);
		vector<LSH*> tmp;
		for (int i = 0; i < masks.size(); i++) {
			tmp.push_back(new LSH(masks[i], bucketsCount, descLength - 2));
		}
		buckets.push_back(tmp);
		curr += angleDist;
	}
}
bool LSHDescriptorFinder::isTypeAcceptable(std::vector<int> *type) {
	throw new std::exception();
}
void LSHDescriptorFinder::add(Descriptor *desc) {
	vector<int> val = desc->getVal();
	for (int j = 0; j < val.size() / descLength; j++) {
		int angle = val[j * descLength];
		if (angle == 360) {
			angle = 359;
		}
		int index = angle / angleDist;
		vector<LSH*> bucket = buckets[index];
		for (int i = 0; i < bucket.size(); i++) {
			bucket[i]->add(desc, j * descLength + 2);
		}
	}
	descList.push_back(desc);
}
void LSHDescriptorFinder::remove(int id) {
	throw new std::exception();
}
Descriptor *LSHDescriptorFinder::find(Descriptor *desc) {
	vector<int> val = desc->getVal();
	vector<int> ids(descList.size());
	for (int i = 0; i < ids.size(); i++) {
		ids[i] = 0;
	}
	for (int i = 0; i < val.size() / descLength; i++) {
		(*Log::ss) << "point id " << i << "\n";
		int angle = val[i * descLength];
		if (angle == 360) {
			angle = 359;
		}
		int index = angle / angleDist;
		int *curVal = &val[i * descLength + 2];
		//TODO bucket[0]
		int id, bestId = -1;
		double dist = 1e300, bestDist = 1e300;
		for (int bucketId = 0; bucketId < buckets[0].size(); bucketId++) {
			LSH *bucket = buckets[index][bucketId];
			id = bucket->getId(curVal, &dist);
			if (dist < bestDist && id != -1) {
				bestDist = dist;
				bestId = id;
			}
			//incIds(bucket->getIds(curVal), &ids);
			if (index < buckets.size() - 1) {
				bucket = buckets[index + 1][bucketId];
				id = bucket->getId(curVal, &dist);
				if (dist < bestDist && id != -1) {
					bestDist = dist;
					bestId = id;
				}
				//incIds(bucket->getIds(curVal), &ids);
			} else {
				bucket = buckets[buckets.size() - 1][bucketId];
				id = bucket->getId(curVal, &dist);
				if (dist < bestDist && id != -1) {
					bestDist = dist;
					bestId = id;
				}
				//incIds(bucket->getIds(curVal), &ids);
			}
			if (index > 0) {
				bucket = buckets[index - 1][bucketId];
				id = bucket->getId(curVal, &dist);
				if (dist < bestDist && id != -1) {
					bestDist = dist;
					bestId = id;
				}
				//incIds(bucket->getIds(curVal), &ids);
			} else {
				bucket = buckets[0][bucketId];
				id = bucket->getId(curVal, &dist);
				if (dist < bestDist && id != -1) {
					bestDist = dist;
					bestId = id;
				}
				//incIds(bucket->getIds(curVal), &ids);
			}
		}
		if (bestId != -1 && dist < threshhold) {
			ids[bestId]++;
		}
	}
	int bestCount = -1, bestId;
	for (int i = 0; i < ids.size(); i++) {
		if (bestCount < ids[i]) {
			bestId = i;
			bestCount = ids[i];
		}
	}
	return descList[bestId];
}
std::vector<Descriptor*> *LSHDescriptorFinder::getDescs() {
	return &descList;
}

void incIds(vector<Descriptor *> *idsToAdd, vector<int> *ids) {
	for (int i = 0; i < idsToAdd->size(); i++) {
		(*ids)[idsToAdd->at(i)->getId()]++;
	}
}

int LSHDescriptorFinder::getMaxId() {
	int maxId = -1;
	for (int i = descList.size() - 1; i >= 0; i--) {
		if (maxId < descList[i]->getId()) {
			maxId = descList[i]->getId();
		}
	}
	return maxId;
}

LSHDescriptorFinder::~LSHDescriptorFinder() {
	for (int i = 0; i < buckets.size(); i++) {
		for (int j = 0; j < buckets[i].size(); j++) {
			delete buckets[i][j];
		}
		buckets[i].clear();
	}
	buckets.clear();
	angleSpaces.clear();
	for (int i = 0; i < descList.size(); i++) {
		delete descList[i];
	}
	descList.clear();
	for (int i = 0; i < masks.size(); i++) {
		for (int j = 0; j < masks[i]->size(); j++) {
			delete masks[i]->at(j);
		}
		delete masks[i];
	}
}