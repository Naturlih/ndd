#include "Descriptor.h"

Descriptor::Descriptor(int id, std::vector<int> val) {
	Descriptor::id = id;
	Descriptor::val = val;
}
int Descriptor::getId() {
	return id;
}
void Descriptor::setId(int id) {
	Descriptor::id = id;
}
std::vector<int> Descriptor::getVal() {
	return val;
}
std::vector<int> *Descriptor::getValPtr() {
	return &val;
}
Descriptor::~Descriptor(void) {
	val.clear();
}