#pragma once
#include "Descriptor.h"
#include "Log.h"
#include <unordered_map>
struct Record{
	Descriptor *descriptor;
	int startIndex;
	Record(Descriptor *desc, int startIndex) {
		Record::descriptor = desc;
		Record::startIndex = startIndex;
	}
};
class LSH
{
public:
	LSH(std::vector<std::vector<int>*> *mask, int bucketsCount, int descLength);
	//void add(int *val, int id);
	void add(Descriptor *d, int indexOfPart);
	int getId(int *desc, double* dist);
	std::vector<Descriptor *> *getIds(int *desc);
	~LSH();
private:
	std::vector<std::vector<int>*> *mask;
	int bucketsCount;
	int zeroHash;
	int descLength;
	std::vector<std::vector<int>*> *hashes;
	std::vector<std::vector<Record>*> *descs;
	std::vector<int> nothing;
	int getBucketId(int *ind);
	int getBucketId(int *ind, int *hash);
	int findBest(int index, int *val, double* dist, int hash);
	double descDistance(int *d1, int *d2, int size);
};
