#pragma once
#include <vector>
class Descriptor
{
public:
	Descriptor(int id, std::vector<int> val);
	int getId();
	void setId(int id);
	std::vector<int> getVal();
	std::vector<int> *getValPtr();
	~Descriptor(void);
private:
	int id;
	std::vector<int> val;
};

