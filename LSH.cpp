#pragma once
#include "LSH.h"
//TODO
#include <sstream>

LSH::LSH(std::vector<std::vector<int>*> *mask, int bucketsCount, int descLength) {
	LSH::mask = mask;
	LSH::descLength = descLength;
	LSH::bucketsCount = bucketsCount;
	/*LSH::buckets = new std::vector<std::vector<int>*>(bucketsCount);
	LSH::bucketVals = new std::vector<std::vector<int*>*>(bucketsCount);
	for (int i = 0; i < bucketsCount; i++) {
		(*buckets)[i] = new std::vector<int>();
		(*bucketVals)[i] = new std::vector<int*>();
	}*/
	hashes = new std::vector<std::vector<int>*>(bucketsCount);
	descs = new std::vector<std::vector<Record>*>(bucketsCount);
	for (int i = 0; i < bucketsCount; i++) {
		(*hashes)[i] = new std::vector<int>();
		(*descs)[i] = new std::vector<Record>();
	}
	//TODO
	int zero[130];
	for (int i = 0; i < 130; i++) {
		zero[i] = 0;
	}
	getBucketId(zero, &zeroHash);
}
/*
void LSH::add(int *val, int id) {
	int bucketId = getBucketId(val);
	if (val[0] == 28 && val[1] == 9) {
		int x = 0;
	}
	if ((*bucketVals)[bucketId]->size() > 0 
		&& (*bucketVals)[bucketId]->at((*bucketVals)[bucketId]->size() - 1) == val) {
		int x = 0;
	}
	(*buckets)[bucketId]->push_back(id);
	(*bucketVals)[bucketId]->push_back(val);
}
void LSH::add(int *val, int id) {
	int bucketId = getBucketId(val);
	if (val[0] == 28 && val[1] == 9) {
		int x = 0;
	}
	if ((*bucketVals)[bucketId]->size() > 0 
		&& (*bucketVals)[bucketId]->at((*bucketVals)[bucketId]->size() - 1) == val) {
		int x = 0;
	}
	(*buckets)[bucketId]->push_back(id);
	(*bucketVals)[bucketId]->push_back(val);
}*/

void LSH::add(Descriptor *d, int indexOfPart) {
	int hash = 0;
	int bucketId = getBucketId(&(d->getValPtr()->at(indexOfPart)), &hash);
	descs->at(bucketId)->push_back(Record(d, indexOfPart));
	hashes->at(bucketId)->push_back(hash);
}

int LSH::getId(int *desc, double* dist) {
	int hash = 0;
	int index = getBucketId(desc, &hash);
	/*if (index == 0) {
		return -1;
	}*/
	if (hash == zeroHash) {
		return -1;
	}
	int id = findBest(index, desc, dist, hash);
	return id;
}
std::vector<Descriptor *> *LSH::getIds(int *desc) {
	int hash = 0;
	int index = getBucketId(desc, &hash);
	//memleak
	std::vector<Descriptor *> *res = new std::vector<Descriptor *>();
	std::vector<Record> *bucket = descs->at(index);
	std::vector<int> *hashBucket = hashes->at(index);
	for (int i = 0; i < bucket->size(); i++) {
		if (hashBucket->at(i) == hash) {
			res->push_back(bucket->at(i).descriptor);
		}
	}

	return res;
}

int LSH::getBucketId(int *desc, int *hhash) {
	unsigned int hash = 0;
	unsigned int part = 1;
	int c = 0;
	std::vector<int> *cShift;
	for (int i = 0; i < LSH::mask->size(); i++) {
		cShift = (*LSH::mask)[i];
		for (int j = 0; j < cShift->size(); j++) {
			int tmp = desc[i];
			tmp = tmp >> (*cShift)[j];
			tmp = tmp & 1;
			part = (part << 1) | ((desc[i] >> (*cShift)[j]) & 1);
			c++;
			//TODO
			if (c % 32 == 31) {
				hash ^= part;
				part = 0;
				c = 0;
			}
			
		}
	}
	if (c != 0) {
		hash ^= part;
	}
	(*hhash) = hash;
	return hash % bucketsCount;
}

int LSH::getBucketId(int *desc) {
	int c = 0;
	return getBucketId(desc, &c);
}

/*
int LSH::findBest(int index, int *val, double* dist) {
	double bestDist = 1e300;
	int bestInd = -1;
	double currDist;
	bool found = false;
	std::vector<int> *trgBucketIds = (*buckets)[index];
	std::vector<int*> *trgBucketVals = (*bucketVals)[index];
	for (int i = 0; i < trgBucketIds->size(); i++) {
		currDist = descDistance(val, (*trgBucketVals)[i], 128);
		if (currDist < bestDist) {
			bestDist = currDist;
			bestInd = (*trgBucketIds)[i];
			found = true;
		}
	}
	*dist = bestDist;
	return bestInd;
}*/

int LSH::findBest(int index, int *val, double* dist, int hash) {
	std::vector<Record> *trgDescs = new std::vector<Record>();
	std::vector<Record> *bucket = descs->at(index);
	std::vector<int> *hashBucket = hashes->at(index);
	for (int i = 0; i < bucket->size(); i++) {
		if (hashBucket->at(i) == hash) {
			trgDescs->push_back(bucket->at(i));
		}
	}

	double bestDist = 1e300;
	int bestInd = -1;
	double currDist;
	bool found = false;
	for (int i = 0; i < trgDescs->size(); i++) {
		currDist = descDistance(val, &((*trgDescs)[i].descriptor->getValPtr()->at((*trgDescs)[i].startIndex)), descLength);
		(*Log::ss) << (*trgDescs)[i].descriptor->getId() << " " << (*trgDescs)[i].startIndex << " " << currDist << "\n";
		if (currDist < bestDist) {
			bestDist = currDist;
			bestInd = trgDescs->at(i).descriptor->getId();
			found = true;
		}
	}
	*dist = bestDist;
	delete trgDescs;
	return bestInd;
}

double LSH::descDistance(int *d1, int *d2, int size) {
	double dist = 0;
	double tmp;
	int c = 0;
	for (int i = 0; i < size; i++) {
		if (d1[i] == 0 || d2[i] == 0) {
			continue;
		}
		c++;
		tmp = d1[i] - d2[i];
		dist += tmp * tmp;
	}
	return dist / c;
}
LSH::~LSH() {
	for (int i = 0; i < hashes->size(); i++) {
		delete hashes->at(i);
	}
	delete hashes;
	for (int i = 0; i < descs->size(); i++) {
		delete descs->at(i);
	}
	delete descs;
}